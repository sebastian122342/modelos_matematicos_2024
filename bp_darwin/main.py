import random as r
import matplotlib.pyplot as plt

def gen_sample(n_org, n_esp):
    sample = []
    for n in range(n_org):
        sample.append(r.randint(1, n_esp))
    return sample

def main():
    # tenemos total_org organismos, en n_esp especies diferentes.

    n_esp = 500 # segun informacion de la página
    priori = 19/122
    total_org = round(n_esp/priori)

    print(f"Simulacion de {total_org} organismos")
    print(f"Con {n_esp} especies diferentes")

    # Generamos una lista de total_org organismos repartidos en n_esp especies
    sample = gen_sample(total_org, n_esp)

    known = []
    n_known = []
    n_unknown = []
    # Ahora hacer el experimento de ir sacando.
    n_iter = total_org * 2
    for i in range(n_iter):
        extracted = r.choice(sample)
        if not extracted in known:
            known.append(extracted)
        n_known.append(len(known))
        n_unknown.append(n_esp-len(known))
    x_values = [i for i in range(1, n_iter+1)]

    print(f"A las 122 iteraciones hay {n_known[119]} especies conocidas")
    print(f"A las 1500 iteraciones hay {n_known[1499]} especies conocidas")

    # Create scatter plot
    plt.figure(figsize=(8, 6))
    plt.scatter(x_values, n_known, color='blue', s=3, label='n° of known Species')
    plt.scatter(x_values, n_unknown, color='red', s=3, label='n° of unknown Species')
    plt.axhline(y=n_esp, color='green', linestyle='-', label=f'n° of species={n_esp}')

    # Add labels and title
    plt.xlabel('Iteration')
    plt.ylabel('Species')
    plt.title('Species vs Iteration')

    # Add legend
    plt.legend()

    # Show plot
    plt.grid(True)  # Optional: add grid
    plt.tight_layout()  # Optional: improve layout
    plt.show()
    

if __name__ == "__main__":
    main()
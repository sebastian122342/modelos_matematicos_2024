# Bonus Point para Modelos Matematicos en Sistemas Biologicos
# Distribucion empírica de puntajes de alineamientos locales aleatorios

import random as r
import numpy as np

import seaborn as sns
import matplotlib.pyplot as plt

import psa

from concurrent.futures import ProcessPoolExecutor
import time


# Generar una secuencia aleatoria a partir de una secuencia original
def gen_rand_seqs(seq):
    seq_list = list(seq)
    r.shuffle(seq_list)
    return "".join(seq_list)


# Sacar la significancia de un score a partir de la lista de scores
def get_pval(whole_data, score):
    # La proporcion de puntajes con puntaje >= a score
    c = 0
    for d in whole_data:
        if d >= score:
            c = c + 1
    return c / len(whole_data)


# Hacer un alineamiento de secuencias water de emboss
def water_score(seq_pair):
    seq1, seq2 = seq_pair
    aln = psa.water(moltype="nucl", qseq=seq1, sseq=seq2)
    return aln.score


# Sacar todos los alineamientos entre las secuencias aleatorias y devolver los scores
def get_scores(rand_seqs):
    scores = []
    for tup in rand_seqs:
        scores.append(water_score(tup))
    return scores


def main():

    start_t = time.time()

    seq1 = "GCTAATCAGTGCATCGATCT"
    seq2 = "GTTGACATCTGCATGCTAGA"

    main_score = water_score((seq1, seq2))
    print(f"Main score: {main_score}")

    # Proceso general:
    # Se parte con un par se secuencias aleatorias, se alinean y se obtiene su puntaje. Con este puntaje, se calcula la significancia de el score inicial. Este p-val se guarda en un arreglo de p-values. También, se calcula la desviacion estandar de los p-values guardados en el arreglo.
    # En la siguiente iteracion, se agregua otro par se secuencias, con estos dos pares de secuencias (el anterior y el generado ahora)volvemos a sacar la significancia del main score, agreguamos el p-val al arreglo de p-val junto a la desviacion estandar de estos p-val en el arreglo.
    # Se repite este proceso 1000 veces (definidos de manera arbitraria) y se aplica un "metodo del codo" para escoger el numero de iteraciones con el que se hara la prueba de significancia final.

    n_iter = 500
    randomseqs = []
    pvals = []
    stdevs = []
    for i in range(1, n_iter):
        ready = False
        while not ready:
            s1, s2 = gen_rand_seqs(seq1), gen_rand_seqs(seq2)
            # añadimos pares de secuencias unicos, evitamos valores repetidos que ensucien los cálculos
            if not (s1, s2) in randomseqs and not (s2, s1) in randomseqs:
                randomseqs.append((s1, s2))
                ready = True
        with ProcessPoolExecutor() as executor:
            scores = list(executor.map(water_score, randomseqs))
        final_pval = get_pval(scores, main_score)
        pvals.append(final_pval)
        stdevs.append(np.std(pvals))

    print(f"desviacion estandar con {n_iter} iteraciones: {np.std(pvals)}")
    print(f"p-value a la {n_iter} iteracion: {pvals[-1]}")

    end_t = time.time()

    print(f"Tiempo de ejecucion para buscar el n° de iteraciones: {end_t-start_t}")

    sns.scatterplot(x=[x for x in range(1, n_iter)], y=stdevs)
    plt.xlabel("Numero de iteración")
    plt.ylabel("Desviación estándar")
    plt.title("Iteraciones vs Desviacion estandar de los p-values")
    plt.show()

    sns.scatterplot(x=[x for x in range(1, n_iter)], y=pvals)
    plt.xlabel("Número de iteración")
    plt.ylabel("p-value")
    plt.title("Iteraciones vs P-value")
    plt.show()


if __name__ == "__main__":
    main()

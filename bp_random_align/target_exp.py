# Hacer el experimento una cantidad determinada de veces

import random as r
import numpy as np

import seaborn as sns
import matplotlib.pyplot as plt

import psa

from concurrent.futures import ProcessPoolExecutor
import time


# Generar una secuencia aleatoria a partir de una secuencia original
def gen_rand_seqs(seq):
    seq_list = list(seq)
    r.shuffle(seq_list)
    return "".join(seq_list)


# Sacar la significancia de un score a partir de la lista de scores
def get_pval(whole_data, score):
    # La proporcion de puntajes con puntaje >= a score
    c = 0
    for d in whole_data:
        if d >= score:
            c = c + 1
    return c / len(whole_data)


# Hacer un alineamiento de secuencias water de emboss
def water_score(seq_pair):
    seq1, seq2 = seq_pair
    aln = psa.water(moltype="nucl", qseq=seq1, sseq=seq2)
    return aln.score


# Sacar todos los alineamientos entre las secuencias aleatorias y devolver los scores
def get_scores(rand_seqs):
    scores = []
    for tup in rand_seqs:
        scores.append(water_score(tup))
    return scores


def main():

    start_t = time.time()

    seq1 = "GCTAATCAGTGCATCGATCT"
    seq2 = "GTTGACATCTGCATGCTAGA"

    main_score = water_score((seq1, seq2))
    print(f"Main score: {main_score}")

    nseqs = 200
    randomseqs = []
    for n in range(nseqs):
        ready = False
        while not ready:
            s1, s2 = gen_rand_seqs(seq1), gen_rand_seqs(seq2)
            # añadimos pares de secuencias unicos, evitamos valores repetidos que ensucien los cálculos
            if not (s1, s2) in randomseqs and not (s2, s1) in randomseqs:
                randomseqs.append((s1, s2))
                ready = True
    scores = get_scores(randomseqs)
    final_pval = get_pval(scores, main_score)

    print(f"p-value: {final_pval}")

    end_t = time.time()
    print(f"Tiempo de ejecucion para el experimento: {end_t-start_t}")

    # Hacer un histograma con scores, marcar en el eje X el main_score y de ahí eñ area hacia la derecha
    ax = sns.histplot(scores, kde=True, color="lightgreen")

    kde_x, kde_y = ax.lines[0].get_data()

    # plotting the two lines
    p1 = plt.axvline(x=main_score, color="red")

    ax.fill_between(
        kde_x,
        kde_y,
        where=(kde_x < 0) | (kde_x >= main_score),
        interpolate=True,
        color="red",
        alpha=0.5,
    )
    ax.fill_between(
        kde_x,
        kde_y,
        where=(kde_x < main_score) | (kde_x > 100),
        interpolate=True,
        color="lightgreen",
        alpha=0.5,
    )

    plt.xlabel("Puntaje")
    plt.ylabel("Frecuencia")
    plt.title("Histograma de puntajes de alineamientos aleatorios")
    plt.legend([f"p-value: {final_pval}"])

    plt.show()


if __name__ == "__main__":
    main()

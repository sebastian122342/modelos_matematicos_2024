from pathlib import Path

import time
import math
from Bio import SeqIO, Entrez
from Bio.SeqUtils import gc_fraction

import pandas as pd

def ls(path):
    """ Se listan todos los elementos de una carpeta """
    return [obj.name for obj in Path(path).iterdir() if obj.is_file()]

# Estimación 1, sin informacion (Igual probabilidad de A T C y G)
def no_info(seq, pattern):
    tinit = time.time()
    prob = len(pattern) * math.pow(0.25, 3) # 3 veces (1/4)^3
    esp = (len(seq)-len(pattern)+1)*prob # Cada patron tiene el mismo tamaño
    tfinish = time.time()
    return [round(esp), tfinish-tinit]

# Usando las frecuencias reales de As, Ts, Cs y Gs
def w_freqs(seq, pattern):
    tinit = time.time()
    freqs = {"A":prob_swindow(seq, "A")[0],
             "T":prob_swindow(seq, "T")[0],
             "C":prob_swindow(seq, "C")[0],
             "G":prob_swindow(seq, "G")[0],}

    prob = 1
    for letter in pattern:
        prob = prob * freqs[letter]
    esp = (len(seq)-len(pattern)+1)*prob
    tfinish = time.time()
    return [round(esp), tfinish-tinit]

# Agregando información del nucleótido anterior (Markov)
def markov(seq, pattern):
    tinit = time.time()
    
    tran_mat = {}
    nucs = ["A", "T", "C", "G"]
    # Inicializar la matriz de transición
    for nuc1 in nucs:
        for nuc2 in nucs:
            tran_mat[nuc1+nuc2] = prob_swindow(seq, nuc1+nuc2)[0]
    
    # Probabilidad del patron
    prob = 1 
    for i in range(len(pattern)-2+1):
        prob = prob * tran_mat[pattern[i:i+2]]
    
    esp = (len(seq)-len(pattern)+1)*prob
    tfinish = time.time()
    return [round(esp), tfinish-tinit]


def all_info(seq, pattern):
    tinit = time.time()
    prob = 0
    prob += prob_swindow(seq, pattern)[0]
    esp = (len(seq)-len(pattern)+1)*prob
    tfinish = time.time()
    return [round(esp), tfinish-tinit]

# Contar cuantas veces se encuentra un patrón en una secuencia usando ventana deslizante
def prob_swindow(seq, pattern):
    tinit = time.time()
    count = 0
    wsize = len(pattern)
    for i in range(len(seq)-wsize+1):
        window = seq[i:i+wsize]
        if window == pattern:
            count += 1
    prob = count/(len(seq)-wsize+1)
    tfinish = time.time()
    return [prob, tfinish-tinit]
    
def rel_err(obs, ref):
    try:
        return ((obs-ref)/ref)*100
    except:
        return "ERR!"


def main():
    genome_dir = "/home/seba/Downloads/all/genomes"
    genomes = ls(genome_dir)

    test = SeqIO.read(genome_dir+"/"+genomes[0], "fasta").seq.upper()

    # son 3 los codones de stop, TAA, TAG, TGA
    patterns = ["TAA", "TAG", "TGA"]
    data = {"name":[],"gc":[],"codon":[], "nreal":[],
            "noinfo":[], "rel_err_noinfo":[], "t_noinfo":[],
            "freqs":[], "rel_err_freqs":[], "t_freqs":[],
            "markov":[], "rel_err_markov":[], "t_markov":[],
            "markov_rev":[], "rel_err_markov_rev":[], "t_markov_rev":[],
            "real":[], "rel_err_real":[], "t_real":[],
            "real_rev":[], "rel_err_real_rev":[], "t_real_rev":[],}
    
    for genome_file in genomes:
        genome = SeqIO.read(genome_dir+"/"+genome_file, "fasta")
        Entrez.email = "sbustamante21@alumnos.utalca.cl"
        acc_number = genome.id
        handle = Entrez.efetch(db="nucleotide", id=acc_number, rettype="rs", retmode="text")

        # Parse the sequence data
        record = handle.read()
        # Parse the record to get the organism information
        for line in record.splitlines():
            if line.startswith("  ORGANISM"):
                sp = line.split("  ORGANISM")[1].strip()
                break
        handle.close()

        seq = genome.seq.upper()
        
        for pattern in patterns:
            gc = gc_fraction(genome)
            wo_info = no_info(seq, pattern)
            wfreq = w_freqs(seq, pattern)
            wmarkov = markov(seq, pattern)
            wmarkov_reversed = markov(seq[::-1], pattern[::-1])
            real_reversed = all_info(seq[::-1], pattern[::-1])
            real = all_info(seq, pattern)
            
            data["name"].append(sp)
            data["gc"].append(gc)
            data["codon"].append(pattern)
            data["nreal"].append(real[0])
            
            data["noinfo"].append(wo_info[0])
            data["rel_err_noinfo"].append(rel_err(wo_info[0], real[0]))
            data["t_noinfo"].append(wo_info[1])

            data["freqs"].append(wfreq[0])
            data["rel_err_freqs"].append(rel_err(wfreq[0], real[0]))
            data["t_freqs"].append(wfreq[1])

            data["markov"].append(wmarkov[0])
            data["rel_err_markov"].append(rel_err(wmarkov[0], real[0]))
            data["t_markov"].append(wmarkov[1])

            data["markov_rev"].append(wmarkov_reversed[0])
            data["rel_err_markov_rev"].append(rel_err(wmarkov_reversed[0], real[0]))
            data["t_markov_rev"].append(wmarkov_reversed[1])

            data["real"].append(real[0])
            data["rel_err_real"].append(rel_err(real[0], real[0]))
            data["t_real"].append(real[1])

            data["real_rev"].append(real_reversed[0])
            data["rel_err_real_rev"].append(rel_err(real_reversed[0], real[0]))
            data["t_real_rev"].append(real_reversed[1])
        break

    df = pd.DataFrame(data)
    df.to_csv("output.csv", index=False, sep="\t", encoding="utf-8")


if __name__ == "__main__":
    main()
